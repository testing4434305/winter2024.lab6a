import java.util.Scanner;
public class LuckyCardGameApp{
	public static void main(String[] args){
		GameManager manager = new GameManager();
		int totalPoints = 0;
		int rounds = 1;
		System.out.println("Hello, we will play a Lucky Card Game now!");
		while(manager.getNumberOfCards()>1 && totalPoints < 5){
			System.out.println(manager);
			totalPoints += manager.calculatePoints();
			System.out.println("Your points are: " + totalPoints);
			manager.dealCards();
			System.out.println("Round " + rounds);
			manager.toString();
			rounds++;
		}
		if(totalPoints>=5){
			System.out.println("You won in the Lucky Card Game!");
		}else{
			System.out.println("You lost.");
		}
		
	}
}