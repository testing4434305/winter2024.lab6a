public class GameManager{
	//fields
	private Deck drawPile;
	private Card centerCard;
	private Card playerCard;
	
	public GameManager(){
		this.drawPile = new Deck();
		this.drawPile.shuffle();
		this.playerCard = drawPile.drawToCard();
		this.centerCard = drawPile.drawToCard();
	}
	public String toString(){
		return "--------------------\nCenter card: " + this.centerCard + "\nPlayer card: " + this.playerCard + "\n--------------------" + "\n***********************";
	}
	//Shuffles and assignes the new values for playerCard and centerCard
	public void dealCards(){
		this.drawPile.shuffle();
		this.playerCard = this.drawPile.drawToCard();
		this.centerCard = this.drawPile.drawToCard();
	}
	//get method for returning amount of cards left in drawPile
	public int getNumberOfCards(){
		return drawPile.length();
	}
	public int calculatePoints(){
		if(this.centerCard.getSuit().equals(this.playerCard.getSuit())){
			return 2;
		}
		else if(this.centerCard.getValue().equals(this.playerCard.getValue())){
			return 4;
		}
		else{
			return -1;
		}
	}
	
}