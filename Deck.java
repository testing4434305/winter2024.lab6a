import java.util.Random;
public class Deck{
	//fields
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	//Assigns amount of cards in deck
	//Creares values for suit and value
	//Places cards in original order of a simple deck
	public Deck(){
		this.numberOfCards = 52;
		this.rng = new Random();
		this.cards = new Card[52];
		String[] suits = {"Hearts", "Diamonds", "Clubs", "Spades"};
		String[] values = {"Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King"};
		int index = 0;
		for(String suit:suits){
			for(String value:values){
				cards[index] = new Card(suit,value);
				index++;
			}
			
		}
	}
	public int length(){
		return numberOfCards;
	}
	public Card drawToCard(){
		this.numberOfCards--;
		return this.cards[numberOfCards];
	}
	public String toString(){
		String allCards = "";
		for(int i=0; i<cards.length; i++){
			allCards += cards[i].toString();
			allCards += ", ";
		}
		return allCards;
	}
	public Card[] shuffle(){
		for(int i =0; i<cards.length-1; i++){
		int randomPosition = rng.nextInt(cards.length - i);
			Card temp = cards[randomPosition];
			cards[randomPosition] = cards[i];
			cards[i] = temp;
		}
		return cards;
	}
}